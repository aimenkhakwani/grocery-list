# _Grocery List_

### _Lists out a grocery list, August 10, 2016_

#### _**By Aimen Khakwani and Ethan Law**_

## Description

_This simple app loops through an array to display a grocery lsit._

##Setup and Installation

* _Clone_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Ethan Law_**
